#!/usr/bin/env python3

#
# @author Arnaud Ceol
#
# Load a dataset in a cbioportal docker instance
#
import docker
import click, configparser,logging

import datetime, time


ROOT_STUDY_FOLDER = "/study"

@click.command(
    context_settings = dict( help_option_names = ['-h', '--help'] )
)
@click.option('--study-folder', '-s',required=True,  help="Name of the folder to load. \
This folder should be available under /study/ in the container. For instance is you specify -c tcga_brca, \
 there should be a folder /study/tcga_brca in the container.")
@click.option('--container-id', '-c',required=True,  help="ID of the container to use. This should be a running container")
@click.option('--mail', '-m',required=True,  help="email to which sending the report")

def main(study_folder, container_id, mail):
    
    ERROR = False

    # from https://stackoverflow.com/a/13891070
    ts = time.time()
    timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d-%H%M%S')


    click.echo("Loading folder %s, timestamp: %s, container: %s" % (study_folder,timestamp,container_id))

    client = docker.from_env()
    
    # upload_folder_path="./" # Should be mounted 
    click.echo("Get container %s" % container_id)
    container = client.containers.get(container_id)

    # Verify that the allowed groups are set
    groups = None
    click.echo("Get groups")
    grep_command = "grep -P '^groups:' %s/%s/meta_study.txt" % (ROOT_STUDY_FOLDER, study_folder)
    exit_code, output = container.exec_run(grep_command)
    if exit_code > 0:
        click.echo("Cannot extract allowed groups, quit without doing anything.")
        click.echo(output)
        ERROR = True
    
    groups = ("%s" % output).split(":")[1].strip()

    
    if groups == None or groups == "":
        click.echo("The groups allowed to access this dataset should be defined in meta_study.txt")
        ERROR = True

    click.echo("Allowed group: %s" % groups)

    # Validate dataset
    # click.echo(container.exec_run("validateData.py --help"))
    click.echo("Start validation")
    validate_command = "validateData.py -s %s/%s -n" % (ROOT_STUDY_FOLDER, study_folder)
    exit_code, output = container.exec_run(validate_command)

    if exit_code != 3 and exit_code != 0:   
        # click.echo(exit_code)
        click.echo(output)
        click.echo("No data wil be loaded")
        return 1


    # Remove previous version
    click.echo("Remove dataset if it exists")
    rm_command = "cbioportalImporter.py -c remove-study  -meta  %s/%s/meta_study.txt" % (ROOT_STUDY_FOLDER, study_folder)
    exit_code, output = container.exec_run(rm_command)

    # if exit_code == 3:   
    #     click.echo("The dataset was not in the database. Skip removing.")  
    if exit_code != 0:
        click.echo("#####################################")
        click.echo("#### FAILED TO LOAD THE DATASET")
        click.echo("#### Error code: %s"% exit_code)
        click.echo("#### Error message: ")
        click.echo(output)
        click.echo("#####################################")
        return 1


    # # load new version
    click.echo("Start loading the dataset")
    load_command = "metaImport.py -n -s %s/%s --html=%s/report_%s.html -v -o" % (ROOT_STUDY_FOLDER, study_folder, ROOT_STUDY_FOLDER, timestamp)
    exit_code, output = container.exec_run(load_command)

    if exit_code != 0:     
        click.echo(exit_code)
        click.echo(output)
    
    # reload cbioportal
    click.echo("Restart the container")
    container.restart()

    # Zip the folder.
    click.echo("Archive the dataset")
    zip_command = "tar czf %s/%s-%s.tgz  -C  %s %s  --remove-files" % (ROOT_STUDY_FOLDER, study_folder, timestamp, ROOT_STUDY_FOLDER, study_folder)
    exit_code, output = container.exec_run(zip_command)

    if exit_code != 0:     
        click.echo(exit_code)
        click.echo(output)
        click.echo("Failed to create tgz archive")
        return 1    

    # send mail
    click.echo("Send mail")
    mail_command = ['sh', '-c', 'echo "cBioPortal dataset %s has been successfully loaded" |  mailx -s "cBioPortal dataset upload report: %s" -A %s/report_%s.html %s' % (study_folder, study_folder, ROOT_STUDY_FOLDER, timestamp, mail)]

    exit_code, output = container.exec_run(mail_command)

    # if exit_code != 0:     
    click.echo(exit_code)
    click.echo(output)
        # click.echo("Failed send email")
        # return 1   
    click.echo("Done")

if __name__ == '__main__':
    main()

