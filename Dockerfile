FROM python:3.7-alpine

COPY requirements.txt .

COPY scripts/* /bin/

COPY README.md .

RUN pip install -r requirements.txt