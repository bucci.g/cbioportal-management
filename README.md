Misc. script for managing cBioPortal data.

# prepare_dataset:

This script create a dataset folder ready to be uploaded in cBioPortal
given the location of of MAF file and a study meta data file.

Usage: `prepare_dataset.py [OPTIONS]`

Options:
```
  -s, --meta-study FILENAME    Path to the meta study file. Example:
                               type_of_cancer: brca
                               cancer_study_identifier:
                               horizon_test_2018
                               name: horizon (TEST 2018)
                               short_name: horizon (TEST)
                               description:horizon
                               samples for testing
                               add_global_case_list: true
                               [required]
  -m, --maf-dir FILENAME       Folder that contains one maf file for each
                               sample to include in the the dataset.
                               [required]
  -t, --template-dir FILENAME  Template folder (inculded with this project)
                               [required]
  -o, --output-dir FILENAME    Path where the dataset folder will be created
                               (will be created if does not exist)  [required]
  -h, --help                   Show this message and exit.
```


## Input:
* Mutations: 1 folder with mutation files (maf). Each file is named after the sample barcode
* Clinical data: TBD
* Meta study file, for instance:

```
type_of_cancer: brca
cancer_study_identifier: horizon_ieo_2018
name: horizon (IEO 2018)
short_name: horizon (IEO)
description:horizon samples for testing
add_global_case_list: true
```


## Processing:
1. extract the patient id, sample id:
    * If the maf file names are barcodes: extract from the maf file names the patient and sample ID
    * in other cases: use the file name as patient and sample ID
2. Merge all the maf files: a simle concatenation of all the files except for the header which should be inserted only once on top of the file
3. Update the template files as described below



## Output:

Folder = "cancer_study_identifier" (e.g.  horizon_ieo_2018)
Files :
* data_clinical_sample.txt: Update the template file, and add all the patient ids extracted from the maf file names.
* meta_clinical_sample.txt: Update the template file
* data_mutations_extended.txt: merged maf files
* meta_mutation_extended: update the template file
* case_list/cases_sequenced.txt:  update the template file, add all patient/sample ids (this is a temporary solution, in the future additional information should be added).

A sample output and the template are available in this repository in the `dataset` folder.

In the future this script may include the maf file generation from vcf, plus quality check, and automatic comparison with previous releases of the dataset.




# Load dataset

This script will load a dataset in a cBioportal instance running in a container. 


## Options:
```
Options:
  -s, --study-folder TEXT  Name of the folder to load. This folder should be
                           available under /study/ in the container. For
                           instance is you specify -c tcga_brca,  there should
                           be a folder /study/tcga_brca in the container.
                           [required]
  -c, --container-id TEXT  ID of the container to use. This should be a
                           running container  [required]
  -m, --mail TEXT          email to which sending the report  [required]
  -h, --help               Show this message and exit.
```

## What the script does:

1. Ensure that the groups allowed to access the data are defined. It the data can be accessed by all users, use the group PUBLIC (or any othe public group defined in your cbioportal proporties). 
2. Validate the dataset (script from the cbioportal conatiner)
3. Remove previous dataset from cbioportal
4. Load the dataset
5. Create a tagged archive (.tgz, tag = timestamp) with the dataset folder.
5. Restart the container
6. Send the validation report by email.

## Where should I put my data.

The dataset should be depositated in a folder mounted as /study/ in the cbioportal container. For instance if the folder on the host machine is `/home/cbioportal/study/`, mount it in the cbioportal's docker-compose.yml as `/home/cbioportal/study/:/study/`. The dataset folder, which contains the meta_study.txt and other files, should be stored as `/home/cbioportal/study/dataset/`. At the end of the script, it will be archived as `/home/cbioportal/study/dataset-DDMMYY-HHMMSS.tgz`


